# @evasuo/packets

[![coverage report](https://gitlab.com/evasuo/uo-packets/badges/master/coverage.svg?ignore_skipped=true)](https://gitlab.com/evasuo/uo-packets/-/commits/master)
[![pipeline status](https://gitlab.com/evasuo/uo-packets/badges/master/pipeline.svg?ignore_skipped=true)](https://gitlab.com/evasuo/uo-packets/-/commits/master)


## Ultima Online packet reader/writer

A tool for building and reading Ultima Online packets.

### Supports

* ✅ NodeJS
* ✅ Browser Javascript

### Example Usage

```javascript
/**
 * Create a packet to tell the client which server to connect to.
 */
import x8CPacket from "@evasuo/uo-packets/packets/x8C";

x8CPacket =  new x8CPacket({gameServerIP: '127.0.0.1', gameServerPort: 2593, key: 1234});
x8CPacket.toBuffer();
```

```javascript
/**
 * Parse a packet that was sent by the server or the client
 */
import Packets from "@evasuo/uo-packets";

/** @var {Buffer} someUOPacket */
const someUOPacket = dataRecieved;
/** The first byte sent by the client or the server is the command */ 
const uoPacketData = Packets.fromBuffer(someUOPacket)

console.log(uoPacketData.toObject()) // ex: {gameServerIP: '127.0.0.1', gameServerPort: 2593, key: 1234}
```

### Contributing Guidelines

1. Use `yarn commit`
1. After committing, `yarn release-beta`, `yarn release-alpha`, or `yarn release`
1. Publishing (limited access) `yarn publish`
