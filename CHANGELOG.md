# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0-beta.17](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.16...v1.0.0-beta.17) (2021-03-19)

## [1.0.0-beta.16](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.15...v1.0.0-beta.16) (2021-03-16)

## [1.0.0-beta.15](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.14...v1.0.0-beta.15) (2021-03-16)

## [1.0.0-beta.14](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.13...v1.0.0-beta.14) (2021-03-16)

## [1.0.0-beta.13](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.12...v1.0.0-beta.13) (2021-03-15)

## [1.0.0-beta.12](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.11...v1.0.0-beta.12) (2021-03-15)

## [1.0.0-beta.11](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.10...v1.0.0-beta.11) (2021-03-15)

## [1.0.0-beta.10](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.9...v1.0.0-beta.10) (2021-03-15)


### Bug Fixes

* **nyc:** fix exclusions filter ([c6ec6d8](https://gitlab.com/evasuo/uo-packets/commit/c6ec6d87bbaceea24e7e35708a43b066159d18a3))

## [1.0.0-beta.9](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.8...v1.0.0-beta.9) (2021-03-15)

## [1.0.0-beta.8](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.7...v1.0.0-beta.8) (2021-03-13)

## [1.0.0-beta.7](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.6...v1.0.0-beta.7) (2021-03-13)


### Features

* **packets:** add xb9 client features packet ([3382698](https://gitlab.com/evasuo/uo-packets/commit/33826988a13732afbf38a75454f8dab605533210))

## [1.0.0-beta.6](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.5...v1.0.0-beta.6) (2021-03-13)


### Features

* **tests:** add good tests and bugfix x8c command ref ([cd456e6](https://gitlab.com/evasuo/uo-packets/commit/cd456e644936d481dbcc498b2857f2df424431b2))
* load UO packet parser from main Packets object ([9c94a78](https://gitlab.com/evasuo/uo-packets/commit/9c94a788e6132ef4f47a37104b378ba59f331278))

## [1.0.0-beta.5](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.4...v1.0.0-beta.5) (2021-03-13)


### Bug Fixes

* **package:** yarn/NPM publishing doctored ([7321902](https://gitlab.com/evasuo/uo-packets/commit/73219023bc56120139c06cc506e82f76e4a233d8))

## [1.0.0-beta.4](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.3...v1.0.0-beta.4) (2021-03-13)


### Bug Fixes

* **npm:** fix npm puiblish ([923a208](https://gitlab.com/evasuo/uo-packets/commit/923a208157689bb317f6a5f62107290077b24dc8))

## [1.0.0-beta.3](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2021-03-13)


### Features

* **compatibility:** add cross broweser/nodejs buffer compatibility ([23a6084](https://gitlab.com/evasuo/uo-packets/commit/23a6084f5b62eee84f25dc3e8826cce935377423))

## [1.0.0-beta.2](https://gitlab.com/evasuo/uo-packets/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-03-13)


### Bug Fixes

* fix npm publishing ([cfc50c3](https://gitlab.com/evasuo/uo-packets/commit/cfc50c38eafb9492662d4956cddb61885528aba2))

## 1.0.0-beta.1 (2021-03-13)


### Bug Fixes

* **test:** make tests pass ([ea8ffc9](https://gitlab.com/evasuo/uo-packets/commit/ea8ffc92a5d00df34b30dc883d758002795a5b2f))

### [1.0.2-beta.0](https://gitlab.com/evasuo/uo-packets/compare/v1.0.1...v1.0.2-beta.0) (2021-03-13)

# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.
