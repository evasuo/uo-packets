import { expect } from "chai";
import Packets from "../src";
import x8CPacket from "../src/packets/x8C";
import AbstractPacket from "./packets/AbstractPacket";

describe("Packets", () => {
    describe("fromBuffer()", () => {
        it("throws error if packet not found", () => {
            expect(() => Packets.fromBuffer(Buffer.from([-255]))).to.throw(
                Error
            );
        });
        it("returns instance of AbstractPacket", () => {
            const result = Packets.fromBuffer(
                new x8CPacket({
                    gameServerPort: 1234,
                    gameServerIp: "0.0.0.0",
                    key: 1234,
                }).toBuffer()
            );
            expect(result).to.be.instanceOf(AbstractPacket);
        });
    });
});
