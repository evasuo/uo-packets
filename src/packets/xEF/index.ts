import AbstractPacket, { PacketDataType } from "../AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface xEFPacketType extends PacketDataType {
    seed: number;
    client: ClientVersionType;
}

export type ClientVersionType = {
    major: number;
    minor: number;
    revision: number;
    prototype: number;
};

/**
 * http://docs.polserver.com/packets/index.php?Packet=0xEF
 * Normally older client send a 4 byte seed (local ip).
 * Newer clients 2.48.0.3+ (KR) and 6.0.5.0+ (2D) are sending
 * this packet.
 */
export default class xEFPacket extends AbstractPacket {
    static command = 0xef;
    static description = "Game client seed";
    static bufferLength = 21;
    readonly bufferLength = xEFPacket.bufferLength;

    constructor(details: xEFPacketType) {
        super(details);
        this.seed = details.seed;
        this.client = details.client;
    }

    private _seed: number;

    get seed(): number {
        return this._seed;
    }

    set seed(value: number) {
        this._seed = value;
    }

    private _client: ClientVersionType;

    get client(): ClientVersionType {
        return this._client;
    }

    set client(value: ClientVersionType) {
        this._client = value;
    }

    static fromBuffer = (buffer: Buffer): xEFPacket => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new xEFPacket({
            seed: buffer.readUInt32BE(1),
            client: {
                major: buffer.readUInt32BE(5),
                minor: buffer.readUInt32BE(9),
                revision: buffer.readUInt32BE(13),
                prototype: buffer.readUInt32BE(17),
            },
        });
    };

    toObject(): xEFPacketType {
        return {
            seed: this.seed,
            client: this.client,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(this.bufferLength, unsafe);
        data.writeUInt8(xEFPacket.command);
        data.writeUInt32BE(this.seed, 1);
        data.writeUInt32BE(this.client.major, 5);
        data.writeUInt32BE(this.client.minor, 9);
        data.writeUInt32BE(this.client.revision, 13);
        data.writeUInt32BE(this.client.prototype, 17);
        return data;
    }
}
