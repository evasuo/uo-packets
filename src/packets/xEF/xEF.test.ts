import { expect } from "chai";
import xEFPacket from "./index";

describe("xEF", () => {
    describe("toBuffer()", () => {
        const sourceData = {
            seed: 1234,
            client: { major: 8, minor: 0, prototype: 0, revision: 0 },
        };
        const source = new xEFPacket(sourceData);
        const result = source.toBuffer();

        it("has expected length", () =>
            expect(result.length).to.eql(xEFPacket.bufferLength));

        it("starts with expected command", () =>
            expect(result.slice(0, 1).readUInt8()).to.eql(xEFPacket.command));

        it("has expected seed", () =>
            expect(result.readUInt32BE(1)).to.eql(sourceData.seed));

        describe("has expected client", () => {
            const { client: expectedClient } = sourceData;
            it("has expected client major version", () =>
                expect(result.readUInt32BE(5)).to.eql(expectedClient.major));

            it("has expected client minor version", () =>
                expect(result.readUInt32BE(9)).to.eql(expectedClient.minor));

            it("has expected client revision version", () =>
                expect(result.readUInt32BE(13)).to.eql(
                    expectedClient.revision
                ));

            it("has expected client prototype version", () =>
                expect(result.readUInt32BE(17)).to.eql(
                    expectedClient.prototype
                ));
        });
    });
    describe("toString()", () => {
        const sourceData = {
            seed: 1234,
            client: { major: 8, minor: 0, prototype: 0, revision: 0 },
        };
        const source = new xEFPacket(sourceData);

        it("is a JSON string", () => {
            expect(
                () => JSON.parse(source.toString()),
                "Expected toString() to be a JSON.stringify() object"
            ).to.not.throw(SyntaxError);
        });

        describe("has expected properties", () => {
            const result = JSON.parse(source.toString());
            it("has expected seed", () => {
                expect(result.seed).to.eql(sourceData.seed);
            });
            it("has expected client", () => {
                expect(result.client).to.eql(sourceData.client);
            });
        });
    });
    describe("toObject()", () => {
        const sourceData = {
            seed: 1234,
            client: { major: 8, minor: 0, prototype: 0, revision: 0 },
        };
        const source = new xEFPacket(sourceData);
        it("is object", () =>
            expect(source.toObject()).to.be.instanceOf(Object));

        describe("has expected properties", () => {
            it("is expected seed", () =>
                expect(source.toObject().seed).to.be.eql(sourceData.seed));
            it("is expected client", () =>
                expect(source.toObject().client).to.be.eql(sourceData.client));
        });
    });

    describe("fromBuffer()", () => {
        const sourceData = {
            seed: 1234,
            client: { major: 8, minor: 0, prototype: 0, revision: 0 },
        };
        const source = new xEFPacket(sourceData);
        const sourceBuffer = source.toBuffer();
        const result = xEFPacket.fromBuffer(sourceBuffer);

        it("should be instance of xEFPacket", () =>
            expect(result).to.be.instanceOf(xEFPacket));

        describe("should have expected values", () => {
            it("should have expected seed", () =>
                expect(result.seed).to.be.eql(sourceData.seed));
            it("should have expected client", () =>
                expect(result.client).to.be.eql(sourceData.client));
        });
    });
});
