import { expect } from "chai";
import x80Packet, { x80PacketType } from "./index";

describe("x80", () => {
    const sourceData: x80PacketType = {
        accountId: "test",
        accountPassword: "testPassword",
        nextLoginKey: 5,
    };
    it("has expected properties", () => {
        const expectedProperties = ["command", "description", "bufferLength"];
        const result = new x80Packet(sourceData);

        const resultPromise = new Promise<x80Packet>((resolve) =>
            resolve(result)
        );

        return resultPromise.then((x80Result) => {
            console.log(x80Result);
            expectedProperties.forEach((expectedProperty) =>
                expect(x80Result).to.haveOwnPropertyDescriptor(expectedProperty)
            );
        });
    });

    describe("toBuffer()", () => {
        const source = new x80Packet(sourceData);
        const result = source.toBuffer();

        it("has expected length", () =>
            expect(result.length).to.eql(x80Packet.bufferLength));

        it("starts with expected command", () =>
            expect(result.slice(0, 1).readUInt8()).to.eql(x80Packet.command));

        const resultAscii = result
            .slice(1, 31)
            .toString("ascii")
            .replace(/\0*$/g, "");
        it("has expected accountID", () =>
            expect(resultAscii).to.eql(sourceData.accountId));

        it("has expected accountPassword", () => {
            const resultAscii = result
                .slice(31, 61)
                .toString("ascii")
                .replace(/\0*$/g, "");
            return expect(resultAscii).to.eql(sourceData.accountPassword);
        });

        it("has expected nextLoginKey", () =>
            expect(result.readUInt8(61)).to.eql(sourceData.nextLoginKey));
    });

    describe("toString()", () => {
        const source = new x80Packet(sourceData);

        it("is a JSON string", () => {
            expect(
                () => JSON.parse(source.toString()),
                "Expected toString() to be a JSON.stringify() object"
            ).to.not.throw(SyntaxError);
        });

        describe("has expected values", () => {
            const result = JSON.parse(source.toString());
            it("has expected accountId", () => {
                expect(result.accountId).to.eql(sourceData.accountId);
            });
            it("has expected accountPassword", () => {
                expect(result.accountPassword).to.eql(
                    sourceData.accountPassword
                );
            });
            it("has expected nextLoginSeed", () => {
                expect(result.nextLoginKey).to.eql(sourceData.nextLoginKey);
            });
        });
    });

    describe("toObject()", () => {
        const source = new x80Packet(sourceData);
        const result = source.toObject();
        it("is object", () =>
            expect(source.toObject()).to.be.instanceOf(Object));

        describe("has expected values", () => {
            it("has expected accountId", () => {
                expect(result.accountId).to.eql(sourceData.accountId);
            });
            it("has expected accountPassword", () => {
                expect(result.accountPassword).to.eql(
                    sourceData.accountPassword
                );
            });
            it("has expected nextLoginSeed", () => {
                expect(result.nextLoginKey).to.eql(sourceData.nextLoginKey);
            });
        });
    });

    function x80Result(sourceData) {
        return new x80Packet(sourceData);
    }

    describe("fromBuffer()", () => {
        const sourceBuffer = x80Result(sourceData).toBuffer();
        const result = x80Packet.fromBuffer(sourceBuffer);

        it("should be instance of x80Packet", () =>
            expect(result).to.be.instanceOf(x80Packet));

        describe("should have expected values", () => {
            it("has expected accountId", () => {
                expect(result.accountId).to.eql(sourceData.accountId);
            });
            it("has expected accountPassword", () => {
                expect(result.accountPassword).to.eql(
                    sourceData.accountPassword
                );
            });
            it("has expected nextLoginSeed", () => {
                expect(result.nextLoginKey).to.eql(sourceData.nextLoginKey);
            });
        });
    });

    describe("accountID", () => {
        it("has character limitations", () => {
            expect(
                () =>
                    (x80Result(sourceData).accountId =
                        "abcdefghijklmnopqrstuvwxyzabcde")
            ).to.throw(Error);
        });
    });

    describe("accountPassword", () => {
        it("has character limitations", () => {
            expect(
                () =>
                    (x80Result(sourceData).accountPassword =
                        "abcdefghijklmnopqrstuvwxyzabcde")
            ).to.throw(Error);
        });
    });

    describe("nextLoginKey", () => {
        it("can be set in constructor", () => {
            const results = x80Result({
                accountId: sourceData.accountId,
                accountPassword: sourceData.accountPassword,
            });

            expect(results.nextLoginKey).to.eql(0);
        });
    });
});
