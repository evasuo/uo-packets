import AbstractPacket, { PacketDataType } from "../AbstractPacket";
import Numbers, { UInt8 } from "../../helper/Numbers";
import Strings, { AsciiStringType, AsciiTrim } from "../../helper/Strings";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface x80PacketType extends PacketDataType {
    accountId: AsciiStringType;
    accountPassword: AsciiStringType;
    nextLoginKey?: UInt8;
}

export default class x80Packet extends AbstractPacket {
    static command = 0x80;
    static description = "Connect to server";
    static bufferLength = 62;
    readonly command = x80Packet.command;
    readonly description = x80Packet.description;
    readonly bufferLength = x80Packet.bufferLength;
    s;

    constructor(details: x80PacketType) {
        super(details);
        this.accountId = details.accountId;
        this.accountPassword = details.accountPassword;
        this.nextLoginKey = details.nextLoginKey
            ? details.nextLoginKey
            : this.nextLoginKey;
    }

    private _accountId: string;

    get accountId(): string {
        return this._accountId;
    }

    set accountId(value: string) {
        if (value.length > 30)
            throw new Error("accountID cannot have more than 30 characters");
        this._accountId = value;
    }

    private _accountPassword: string;

    get accountPassword(): string {
        return this._accountPassword;
    }

    set accountPassword(value: string) {
        if (value.length > 30)
            throw new Error("accountID cannot have more than 30 characters");
        this._accountPassword = value;
    }
    // ⛔ ️_nextLoginKey - Apparently a dead property.

    // Meant to represent NextLoginKey value found in UO.cfg (no longer present)
    private _nextLoginKey = 0;

    get nextLoginKey() {
        return this._nextLoginKey;
    }

    set nextLoginKey(value: number) {
        Numbers.ValidateUInt8(value);
        this._nextLoginKey = Numbers.toUInt8(value);
    }

    static fromBuffer = (buffer: Buffer): x80Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new x80Packet({
            accountId: AsciiTrim(buffer.slice(1, 31).toString("ascii")),
            accountPassword: AsciiTrim(buffer.slice(31, 61).toString("ascii")),
            nextLoginKey: buffer.readUInt8(61),
        });
    };

    toObject(): x80PacketType {
        return {
            accountId: this.accountId,
            accountPassword: this.accountPassword,
            nextLoginKey: this.nextLoginKey,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(
            x80Packet.bufferLength,
            unsafe
        );
        data.writeUInt8(x80Packet.command);

        data.write(this.accountId, 1, 30);
        data.write(this.accountPassword, 31, 30);
        data.writeUInt8(this.nextLoginKey, 61);

        return data;
    }
}
