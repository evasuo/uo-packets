import AbstractPacket, { PacketDataType } from "../AbstractPacket";
import Numbers, { UInt32 } from "../../helper/Numbers";
import { AsciiStringType, AsciiTrim } from "../../helper/Strings";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface x91PacketType extends PacketDataType {
    authID: UInt32;
    accountId: AsciiStringType;
    accountPassword: AsciiStringType;
}

export default class x91Packet extends AbstractPacket {
    static command = 0x91;
    static description = "Game server login";
    static bufferLength = 65;
    readonly command = x91Packet.command;
    readonly description = x91Packet.description;
    readonly bufferLength = x91Packet.bufferLength;

    constructor(details: x91PacketType) {
        super(details);
        this.authId = details.authID;
        this.accountId = details.accountId;
        this.accountPassword = details.accountPassword.toString();
    }

    private _authId: UInt32;

    get authId(): UInt32 {
        return this._authId;
    }

    set authId(value: UInt32) {
        this._authId = Numbers.toUInt32(value);
    }

    private _accountId: AsciiStringType;

    get accountId(): AsciiStringType {
        return this._accountId;
    }

    set accountId(value: AsciiStringType) {
        if (typeof value !== "string" || value.trim().length === 0)
            throw new Error("accountID cannot be empty");
        if (value.length > 30)
            throw new Error("accountId exceeded character limit of 30");
        this._accountId = value;
    }

    private _accountPassword: AsciiStringType;

    get accountPassword(): AsciiStringType {
        return this._accountPassword;
    }

    set accountPassword(value: AsciiStringType) {
        if (typeof value !== "string" || value.trim().length === 0)
            throw new Error("accountPassword cannot be empty");
        if (value.length > 30)
            throw new Error("accountPassword exceeded character limit of 30");
        this._accountPassword = value;
    }

    static fromBuffer = (buffer: Buffer): x91Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new x91Packet({
            authID: buffer.readUInt32BE(1),
            accountId: AsciiTrim(buffer.slice(5, 35).toString("ascii")),
            accountPassword: AsciiTrim(buffer.slice(35, 65).toString("ascii")),
        });
    };

    toObject(): x91PacketType {
        return {
            authID: this.authId,
            accountId: this.accountId,
            accountPassword: this.accountPassword,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(
            x91Packet.bufferLength,
            unsafe
        );
        data.writeUInt8(x91Packet.command);
        data.writeUInt32BE(this.authId, 1);
        data.write(this.accountId, 5, 35);
        data.write(this.accountPassword, 35, 65);

        return data;
    }
}
