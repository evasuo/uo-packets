import { expect } from "chai";
import x91Packet, { x91PacketType } from "./index";
import { AsciiTrim } from "../../helper/Strings";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer;

describe("x91", () => {
    function getResult(srcData: x91PacketType) {
        return new x91Packet(srcData);
    }
    const srcData = {
        authID: 1234,
        accountId: "Test",
        accountPassword: "TestPassword",
    };
    it("has single-byte command", () =>
        expect(Buffer.from([x91Packet.command]).length).to.eql(1));

    it("toObject()", () => {
        expect(getResult(srcData).toObject()).to.eql(srcData);
    });

    it("toString()", () => {
        const resultPromise = new Promise<x91Packet>((resolve) =>
            resolve(getResult(srcData))
        );

        return resultPromise.then((result) => {
            expect(result.toString()).to.eql(JSON.stringify(result.toObject()));
        });
    });

    it("toBuffer()", () => {
        const resultPromise = new Promise<x91Packet>((resolve) =>
            resolve(getResult(srcData))
        );

        return resultPromise.then((result) => {
            const buffer = result.toBuffer();
            expect(
                buffer,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(CrossCompatibleBuffer);

            expect(
                buffer.readUInt8(0),
                "First byte should match command"
            ).to.eql(x91Packet.command);

            expect(
                buffer.readUInt32BE(1),
                "AuthID in buffer does not match what was provided"
            ).to.eql(srcData.authID);

            expect(
                AsciiTrim(buffer.slice(5, 35).toString("ascii")),
                "accountID in buffer does not match what was provided"
            ).to.eql(srcData.accountId);

            expect(
                AsciiTrim(buffer.slice(35, 65).toString("ascii")),
                "accountPassword in buffer does not match what was provided"
            ).to.eql(srcData.accountPassword);
        });
    });

    it("fromBuffer()", () => {
        const resultPromise = new Promise<x91Packet>((resolve) =>
            resolve(getResult(srcData))
        );

        return resultPromise.then((result) => {
            const srcBuffer = result.toBuffer();
            const x91Data = x91Packet.fromBuffer(srcBuffer);

            expect(
                x91Data,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(x91Packet);

            expect(x91Data.command, "First byte should match command").to.eql(
                x91Packet.command
            );

            expect(
                x91Data.authId,
                "Reason in data does not match what was provided"
            ).to.eql(srcData.authID);

            expect(
                x91Data.accountId,
                "accountId in data does not match what was provided"
            ).to.eql(srcData.accountId);

            expect(
                x91Data.accountPassword,
                "accountId in data does not match what was provided"
            ).to.eql(srcData.accountPassword);
        });
    });

    describe("accountId", () => {
        it("cannot be empty", () => {
            expect(
                () => getResult({ ...srcData, accountId: "" }).accountId
            ).to.throw("accountID cannot be empty");
        });
        it("cannot be too long", () => {
            expect(
                () =>
                    getResult({
                        ...srcData,
                        accountId:
                            "asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf",
                    }).accountId
            ).to.throw("accountId exceeded character limit of 30");
        });
    });

    describe("accountPassword", () => {
        it("cannot be empty", () => {
            expect(
                () =>
                    getResult({ ...srcData, accountPassword: "" })
                        .accountPassword
            ).to.throw("accountPassword cannot be empty");
        });
        it("cannot be too long", () => {
            expect(
                () =>
                    getResult({
                        ...srcData,
                        accountPassword:
                            "asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdf",
                    }).accountPassword
            ).to.throw("accountPassword exceeded character limit of 30");
        });
    });
});
