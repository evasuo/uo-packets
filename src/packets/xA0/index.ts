import AbstractPacket, { PacketDataType } from "../AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface xA0PacketType extends PacketDataType {
    serverIndex: number;
}

export default class xA0Packet extends AbstractPacket {
    static command = 0xa0;
    readonly command = xA0Packet.command;
    static description = "Server selected";
    readonly description = xA0Packet.description;

    get serverIndex(): number {
        return this._serverIndex;
    }
    set serverIndex(value: number) {
        this._serverIndex = value;
    }
    private _serverIndex: number;

    constructor(details: xA0PacketType) {
        super(details);
        this.serverIndex = details.serverIndex;
    }

    toObject(): xA0PacketType {
        return {
            serverIndex: this.serverIndex,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(3, unsafe);
        data.writeUInt8(xA0Packet.command);
        data.writeUInt16BE(this.serverIndex, 1);

        return data;
    }

    static fromBuffer = (buffer: Buffer): xA0Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new xA0Packet({
            serverIndex: buffer.readUInt16BE(1),
        });
    };
}
