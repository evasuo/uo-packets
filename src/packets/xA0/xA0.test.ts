import { expect } from "chai";
import xA0Packet from "./index";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer;

describe("xA0", () => {
    it("has single-byte command", () =>
        expect(Buffer.from([xA0Packet.command]).length).to.eql(1));

    it("toObject()", () => {
        const serverIndex = 50;
        const xA0 = new xA0Packet({ serverIndex: serverIndex });
        const resultPromise = new Promise<xA0Packet>((resolve) => resolve(xA0));

        return resultPromise.then((result) => {
            expect(result.toObject()).to.haveOwnProperty("serverIndex");
            expect(result.toObject().serverIndex).to.eql(serverIndex);
        });
    });

    it("toString()", () => {
        const serverIndex = 50;
        const xA0 = new xA0Packet({ serverIndex: serverIndex });
        const resultPromise = new Promise<xA0Packet>((resolve) => resolve(xA0));

        return resultPromise.then((result) => {
            expect(result.toString()).to.eql(JSON.stringify(result.toObject()));
        });
    });

    it("toBuffer()", () => {
        const serverIndex = 50;
        const xA0 = new xA0Packet({ serverIndex: serverIndex });
        const resultPromise = new Promise<xA0Packet>((resolve) => resolve(xA0));

        return resultPromise.then((result) => {
            const buffer = result.toBuffer();
            expect(
                buffer,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(CrossCompatibleBuffer);

            expect(
                buffer.readUInt8(0),
                "First byte should match command"
            ).to.eql(xA0Packet.command);

            expect(
                buffer.readUInt16BE(1),
                "Server number in buffer does not match"
            ).to.eql(serverIndex);
        });
    });

    it("fromBuffer()", () => {
        const serverIndex = 50;
        const xA0 = new xA0Packet({ serverIndex: serverIndex });
        const resultPromise = new Promise<xA0Packet>((resolve) => resolve(xA0));

        return resultPromise.then((result) => {
            const srcBuffer = result.toBuffer();
            const xA0Data = xA0Packet.fromBuffer(srcBuffer);

            expect(
                xA0Data,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(xA0Packet);

            expect(xA0Data.command, "First byte should match command").to.eql(
                xA0Packet.command
            );

            expect(xA0Data.serverIndex, "Server index does not match").to.eql(
                serverIndex
            );
        });
    });
});
