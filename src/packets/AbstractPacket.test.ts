import { expect } from "chai";
import AbstractPacket from "./AbstractPacket";

describe("AbstractPacket.fromBuffer", () => {
    it("Is defined", () => {
        const defaultFromBuffer = AbstractPacket.fromBuffer(
            Buffer.from([0, 0, 0, 0])
        );

        expect(AbstractPacket).to.haveOwnProperty("fromBuffer");
    });
});

describe("AbstractPacket.toBuffer", () => {
    it("Is defined", () => {
        expect(AbstractPacket).to.haveOwnProperty("toBuffer");
    });

    it("Returns default value", () => {
        expect(AbstractPacket.toBuffer().readUInt8()).to.eql(
            Buffer.from([-1]).readUInt8()
        );
    });
});

describe("AbstractPacket.createBuffer", () => {
    it("Is defined", () => {
        expect(AbstractPacket).to.haveOwnProperty("createBuffer");
    });

    it("Returns expected safe buffer", () => {
        expect(AbstractPacket.createBuffer(3, false).byteLength).to.eql(3);
    });

    it("Returns expected unsafe buffer", () => {
        expect(AbstractPacket.createBuffer(3, true).byteLength).to.eql(3);
    });
});
