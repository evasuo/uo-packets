import { expect } from "chai";
import ServerItem from "./ServerItem";
import { Buffer as NodeWebBuffer } from "buffer/";

export const testServerItemBuffer = (
    serverItemBuffer: Buffer | NodeWebBuffer,
    sourceData: {
        percentFull: number;
        timezone: number;
        index: number;
        serverName: string;
        serverIP: string;
    }
) => {
    expect(
        serverItemBuffer.readUInt16BE(0),
        "Servers index should match"
    ).to.eql(sourceData.index);

    expect(
        sourceData.serverName,
        "Server name should be included in label"
    ).to.contain(
        serverItemBuffer.slice(2, 34).toString("utf8").replace(/\0*$/g, "")
    );
    expect(
        [
            serverItemBuffer.readUInt8(36),
            serverItemBuffer.readUInt8(37),
            serverItemBuffer.readUInt8(38),
            serverItemBuffer.readUInt8(39),
        ]
            .reverse()
            .join("."),
        "IP should match"
    ).to.eql(sourceData.serverIP);

    expect(serverItemBuffer.readUInt8(34), "Percent full should match").to.eql(
        sourceData.percentFull
    );

    expect(serverItemBuffer.readUInt8(35), "Timezone should match").to.eql(
        sourceData.timezone
    );
};

describe("xA8 - Server Item", () => {
    it("toObject()", () => {
        const sourceData = {
            index: 20,
            serverName: "Red hot applesauce",
            serverIP: "127.0.0.1",
            percentFull: 50,
            timezone: 255,
        };
        const serverItem = new ServerItem(sourceData);

        expect(serverItem.toObject()).to.eql(sourceData);
    });
    it("toString()", () => {
        const sourceData = {
            index: 20,
            serverName: "Red hot applesauce",
            serverIP: "127.0.0.1",
            percentFull: 50,
            timezone: 255,
        };
        const serverItem = new ServerItem(sourceData);

        expect(JSON.parse(serverItem.toString())).to.eql(sourceData);
    });

    it("toBuffer()", () => {
        const sourceData = {
            index: 20,
            serverName: "Red hot apple sauce",
            serverIP: "127.0.0.1",
            percentFull: 50,
            timezone: 254,
        };
        const resultPromise = new Promise<ServerItem>((resolve) =>
            resolve(new ServerItem(sourceData))
        );

        return resultPromise.then((result) => {
            const serverItemBuffer = result.toBuffer();
            testServerItemBuffer(serverItemBuffer, sourceData);
        });
    });

    it("fromBuffer()", () => {
        const sourceData = {
            index: 20,
            serverName: "Red hot apple sauce",
            serverIP: "127.0.0.1",
            percentFull: 50,
            timezone: 254,
        };
        const resultPromise = new Promise<ServerItem>((resolve) => {
            const serverItem = new ServerItem(sourceData);
            resolve(ServerItem.fromBuffer(serverItem.toBuffer()));
        });

        return resultPromise.then((result) => {
            const resultDetail = result.toObject();
            expect(result, "Should be ServerItem instance").to.be.instanceOf(
                ServerItem
            );

            expect(
                sourceData.serverName,
                "Server name should be included in label"
            ).to.contain(resultDetail.serverName);

            expect(
                resultDetail.percentFull,
                "Percent full should match"
            ).to.eql(sourceData.percentFull);

            expect(resultDetail.timezone, "Timezone full should match").to.eql(
                sourceData.timezone
            );

            expect(resultDetail.serverIP, "Server IP should match").to.eql(
                sourceData.serverIP
            );

            expect(resultDetail.index, "Index should match").to.eql(
                sourceData.index
            );
        });
    });
});
