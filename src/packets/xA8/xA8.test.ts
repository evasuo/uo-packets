import { expect } from "chai";
import xA8Packet, { xA8PacketConstructor, xA8PacketType } from "./index";
import ServerItem from "./ServerItem";
import { testServerItemBuffer } from "./ServerItem.test";

describe("xA8", () => {
    const serverOne = new ServerItem({
        index: 0,
        serverIP: "1.2.3.4",
        percentFull: 50,
        serverName: "Monkey Sauce",
        timezone: 123,
    });
    const serverTwo = new ServerItem({
        index: 1,
        serverIP: "10.20.30.40",
        percentFull: 50,
        serverName: "Monkey Sauce",
        timezone: 123,
    });
    const sourceData: xA8PacketConstructor = {
        servers: [serverOne, serverTwo],
    };
    describe("has server limit", () => {
        const UInt16Limit = 65535;
        const servers = [];
        for (
            let serverCount = 0;
            serverCount * 40 + 6 < UInt16Limit;
            serverCount++
        )
            servers.push(serverOne);

        expect(() => new xA8Packet({ servers })).to.throw(
            "Invalid length. Too many servers may have been added."
        );
    });
    describe("toBuffer()", () => {
        const source = new xA8Packet(sourceData);
        const result = source.toBuffer();
        const expectedLength = 6 + sourceData.servers.length * 40;

        it("has expected buffer length", () => {
            expect(result.length).to.eql(expectedLength);
        });

        describe("Has expected header data", () => {
            it("starts with expected command", () =>
                expect(result.readUInt8(0)).to.eql(xA8Packet.command));

            it("has expected buffer length value", () =>
                expect(result.readUInt16BE(1)).to.eql(expectedLength));

            it("has expected system flag", () =>
                expect(result.readUInt8(3)).to.eql(source.systemFlag));

            it("has expected system flag", () =>
                expect(result.readUInt16BE(4)).to.eql(
                    sourceData.servers.length
                ));
        });

        describe("Has expected server data", () => {
            it("has server one data", () => {
                const resultPromise = new Promise((resolve) => {
                    resolve(result.slice(6, 46));
                });

                return resultPromise.then((serverOneBuffer: Buffer) => {
                    testServerItemBuffer(serverOneBuffer, serverOne.toObject());
                });
            });
            it("has server two data", () => {
                const resultPromise = new Promise((resolve) => {
                    resolve(result.slice(46, result.length));
                });

                return resultPromise.then((serverTwoBuffer: Buffer) => {
                    testServerItemBuffer(serverTwoBuffer, serverTwo.toObject());
                });
            });
        });

        // describe("has expected client", () => {
        //     const { client: expectedClient } = sourceData;
        //     it("has expected client major version", () =>
        //         expect(result.readUInt32BE(5)).to.eql(expectedClient.major));
        //
        //     it("has expected client minor version", () =>
        //         expect(result.readUInt32BE(9)).to.eql(expectedClient.minor));
        //
        //     it("has expected client revision version", () =>
        //         expect(result.readUInt32BE(13)).to.eql(
        //             expectedClient.revision
        //         ));
        //
        //     it("has expected client prototype version", () =>
        //         expect(result.readUInt32BE(17)).to.eql(
        //             expectedClient.prototype
        //         ));
        // });
    });
    describe("toString()", () => {
        const source = new xA8Packet(sourceData);

        it("is a JSON string", () => {
            expect(
                () => JSON.parse(source.toString()),
                "Expected toString() to be a JSON.stringify() object"
            ).to.not.throw(SyntaxError);
        });

        const result = JSON.parse(source.toString());
        describe("has expected headers", () => {
            it("has expected length", () =>
                expect(result.length).to.eql(
                    6 + 40 * sourceData.servers.length
                ));

            it("has expected systemFlag", () =>
                expect(result.systemFlag).to.eql(
                    xA8Packet.FLAG_GENERAL_SERVER_DEFAULT
                ));
            it("has expected serverCount", () =>
                expect(result.serverCount).to.eql(sourceData.servers.length));

            it("has expected server data", () =>
                expect(result.servers).to.eql([
                    serverOne.toObject(),
                    serverTwo.toObject(),
                ]));
        });
    });
    describe("toObject()", () => {
        const source = new xA8Packet(sourceData);
        it("is object", () =>
            expect(source.toObject()).to.be.instanceOf(Object));

        it("has expected properties", () =>
            expect(source.toObject()).to.be.eql({
                length: 6 + 40 * sourceData.servers.length,
                serverCount: sourceData.servers.length,
                systemFlag: xA8Packet.FLAG_GENERAL_SERVER_DEFAULT,
                servers: [serverOne.toObject(), serverTwo.toObject()],
            }));
    });

    describe("fromBuffer()", () => {
        const source = new xA8Packet(sourceData);
        const sourceBuffer = source.toBuffer();
        const result = xA8Packet.fromBuffer(sourceBuffer);

        it("should be instance of xA8Packet", () =>
            expect(result).to.be.instanceOf(xA8Packet));

        it("source and new buffer should match", () =>
            expect(result.toBuffer().buffer).to.be.eql(sourceBuffer.buffer));
        it("result should contain buffer data", () =>
            expect(result).to.be.eql(source));
    });
});
