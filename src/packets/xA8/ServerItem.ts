import AbstractPacket from "../AbstractPacket";
import Numbers from "../../helper/Numbers";
import { Buffer as NodeWebBuffer } from "buffer/";

export type ServerItemDataType = {
    index: number;
    serverName: string;
    percentFull: number;
    timezone: number;
    serverIP: string;
};

export default class ServerItem extends AbstractPacket {
    static command = undefined;
    readonly command = ServerItem.command;
    static description = "Server List Item";
    readonly description = ServerItem.description;
    get ipAddress(): string {
        return this._ipAddress;
    }

    set ipAddress(value: string) {
        this._ipAddress = value;
    }
    get timezone(): number {
        return this._timezone;
    }

    set timezone(value: number) {
        Numbers.ValidateUInt8(value);
        this._timezone = value;
    }
    get name(): string {
        return this._name;
    }

    set name(value: string) {
        function trimTrailingEmptyAscii() {
            return value.replace(/\0*$/g, "");
        }

        this._name = trimTrailingEmptyAscii().trimEnd();
    }
    get index(): number {
        return this._index;
    }

    set index(value: number) {
        this._index = value;
    }
    get percentFull(): number {
        return this._percentFull;
    }

    set percentFull(value: number) {
        this._percentFull = value;
    }
    private _index: number;
    private _name: string;
    private _percentFull: number;
    private _timezone: number;
    private _ipAddress: string;

    constructor(details: ServerItemDataType) {
        super(details);
        this.index = details.index;
        this.name = details.serverName;
        this.percentFull = details.percentFull;
        this.timezone = details.timezone;
        this.ipAddress = details.serverIP;
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toObject(): ServerItemDataType {
        return {
            index: this.index,
            serverName: this.name,
            percentFull: this.percentFull,
            timezone: this.timezone,
            serverIP: this.ipAddress,
        };
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(40, unsafe);
        const serverLabel = Buffer.from(this.name)
            .slice(0, 32) // min/max chars is 32
            .toString("utf8");
        const ipParts = this.ipAddress
            .split(".")
            .reverse()
            .map((ipPart) => parseInt(ipPart));

        data.writeUInt16BE(this.index);
        data.write(serverLabel, 2, "utf8");
        data.writeUInt8(this.percentFull, 34);
        data.writeUInt8(this.timezone, 35);
        data.writeUInt8(ipParts[0], 36);
        data.writeUInt8(ipParts[1], 37);
        data.writeUInt8(ipParts[2], 38);
        data.writeUInt8(ipParts[3], 39);

        return data;
    }

    static fromBuffer(data: NodeWebBuffer | Buffer): ServerItem {
        return new ServerItem({
            index: data.readUInt16BE(0),
            serverName: data.slice(2, 34).toString(),
            percentFull: data.readUInt8(34),
            timezone: data.readUInt8(35),
            serverIP: [
                data.readUInt8(36),
                data.readUInt8(37),
                data.readUInt8(38),
                data.readUInt8(39),
            ]
                .reverse()
                .join("."),
        });
    }
}
