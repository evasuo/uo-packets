import ServerItem, { ServerItemDataType } from "./ServerItem";
import AbstractPacket, { PacketDataType } from "../AbstractPacket";
import Numbers from "../../helper/Numbers";
import { Buffer as NodeWebBuffer } from "buffer/";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export type ServerList = ServerItem[];

export type systemFlagType = 0xcc | 0x64 | 0x5d | number;

export interface xA8PacketType extends PacketDataType {
    length: number;
    systemFlag: systemFlagType;
    serverCount: number;
    servers: ServerList;
}
export interface xA8PacketConstructor extends PacketDataType {
    systemFlag?: systemFlagType;
    servers: ServerList;
}
export type xA8PacketTypeFlat = Omit<xA8PacketType, "servers"> & {
    servers: ServerItemDataType[];
};

export default class xA8Packet extends AbstractPacket {
    static command = 0xa8;
    readonly command = xA8Packet.command;

    static description = "Servers list";
    readonly description = xA8Packet.description;

    static FLAG_DO_NOT_SEND_VIDEO_CARD_INFO: systemFlagType = 0xcc;
    readonly FLAG_DO_NOT_SEND_VIDEO_CARD_INFO =
        xA8Packet.FLAG_DO_NOT_SEND_VIDEO_CARD_INFO;

    static FLAG_SEND_VIDEO_CARD_INFO: systemFlagType = 0x64;
    readonly FLAG_SEND_VIDEO_CARD_INFO = xA8Packet.FLAG_SEND_VIDEO_CARD_INFO;

    static FLAG_GENERAL_SERVER_DEFAULT: systemFlagType = 0x5d;
    readonly FLAG_GENERAL_SERVER_DEFAULT =
        xA8Packet.FLAG_GENERAL_SERVER_DEFAULT;

    constructor(details: xA8PacketConstructor) {
        super(details);
        if (details.systemFlag) this.systemFlag = details.systemFlag;
        this.servers = details.servers;
    }

    get serverCount(): number {
        return this.servers.length;
    }

    get length(): number {
        const resultingBufferLength = 6 + 40 * this.servers.length;
        Numbers.ValidateUInt16(resultingBufferLength);
        return Numbers.toUInt16(resultingBufferLength);
    }

    private _systemFlag: systemFlagType = xA8Packet.FLAG_GENERAL_SERVER_DEFAULT;

    get systemFlag(): systemFlagType {
        return this._systemFlag;
    }

    set systemFlag(value: systemFlagType) {
        Numbers.ValidateUInt8(value);
        this._systemFlag = Numbers.toUInt8(value);
    }

    private _servers: ServerList;

    get servers(): ServerList {
        return this._servers;
    }

    set servers(value: ServerList) {
        const originalValue = [].concat(this._servers);
        this._servers = value;
        try {
            this.length;
        } catch (error) {
            this._servers = originalValue;
            throw new Error(
                "Invalid length. Too many servers may have been added."
            );
        }
    }

    static fromBuffer = (buffer: NodeWebBuffer | Buffer): xA8Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        const serverCount = buffer.readUInt16BE(4);
        const servers: ServerList = [];
        const serversBuffer = buffer.slice(6, buffer.length);
        for (let count = 1; count <= serverCount; count++) {
            const serverStart = count * 40 - 40;
            const serverEnd = count * 40;
            servers.push(
                ServerItem.fromBuffer(
                    serversBuffer.slice(serverStart, serverEnd)
                )
            );
        }

        return new xA8Packet({
            systemFlag: buffer.readUInt8(3),
            servers,
        });
    };

    toObject(): xA8PacketTypeFlat {
        return {
            length: this.length,
            systemFlag: this.systemFlag,
            serverCount: this.serverCount,
            servers: this.servers.map((server) => server.toObject()),
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(): NodeWebBuffer | Buffer {
        const buffer = NodeWebBuffer.alloc(this.length);
        const headerBuffer = this.getHeaderBuffer();
        const serversBuffer = this.getServersBuffer();

        headerBuffer.copy(buffer, 0, 0, headerBuffer.length);
        serversBuffer.copy(
            buffer,
            headerBuffer.length,
            0,
            serversBuffer.length
        );

        return buffer;
    }

    private getServersBuffer() {
        const ServersBuffer = this.servers.map<Buffer>((server) =>
            server.toBuffer()
        );

        return NodeWebBuffer.concat(ServersBuffer);
    }

    private getHeaderBuffer() {
        const headerBuffer = NodeWebBuffer.alloc(6);
        headerBuffer.writeUInt8(xA8Packet.command, 0);
        headerBuffer.writeUInt16BE(this.length, 1);
        headerBuffer.writeUInt8(this.systemFlag, 3);
        headerBuffer.writeUInt16BE(this.servers.length, 4);
        return headerBuffer;
    }
}
