// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

interface PacketInterface {
    readonly command: number;
    [key: string]: any;
}

export interface PacketDataType {
    [key: string]: any;
}

export default abstract class AbstractPacket implements PacketInterface {
    // This is out of range for a byte, therefore will throw an error if attempted to write to a byte.
    readonly command: number = -129;
    readonly description: string = "Packet not described";
    protected constructor(details: PacketDataType) {
        // Do Nothing
    }
    abstract toString(): string;
    abstract toObject(): Record<string, unknown>;
    static fromBuffer(buffer: Buffer): PacketInterface {
        return { command: -1, status: "Method not implemented", buffer };
    }
    static toBuffer(unsafe = false): Buffer {
        return CrossCompatibleBuffer.from([-1]);
    }

    static createBuffer(size: number, unsafe: boolean): Buffer {
        let data: Buffer;
        if (unsafe) data = CrossCompatibleBuffer.allocUnsafe(size);
        else data = CrossCompatibleBuffer.alloc(size);

        return data;
    }
}
