import { expect } from "chai";
import x8CPacket from "./index";
import Packets from "../../index";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer;

describe("x8C", () => {
    it("Can load packet from buffer", () => {
        const details = {
            gameServerIp: "127.0.0.1",
            gameServerPort: 2593,
            key: 123,
        };
        const x8CData = new Packets[x8CPacket.command](details);

        expect(Packets.fromBuffer(x8CData.toBuffer()).toObject()).to.eql(
            details
        );
    });
    it("toString()", () => {
        const xA0 = new x8CPacket({
            gameServerIp: "127.0.0.1",
            gameServerPort: 2593,
            key: 1234,
        });
        expect(xA0.toString()).to.eql(JSON.stringify(xA0.toObject()));
    });
});
