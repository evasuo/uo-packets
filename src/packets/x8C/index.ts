import AbstractPacket, { PacketDataType } from "../AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface x8CPacketType extends PacketDataType {
    gameServerIp: string;
    gameServerPort: number;
    key: number;
}

export default class x8CPacket extends AbstractPacket {
    static command = 0x8c;
    static description = "Connect to server";

    get key(): number {
        return this._key;
    }
    set key(value: number) {
        this._key = value;
    }

    get gameServerPort(): number {
        return this._gameServerPort;
    }
    set gameServerPort(value: number) {
        this._gameServerPort = value;
    }

    get gameServerIp(): string {
        return this._gameServerIp;
    }
    set gameServerIp(value: string) {
        this._gameServerIp = value;
    }
    private _gameServerIp: string;
    private _gameServerPort: number;
    private _key;
    constructor(details: x8CPacketType) {
        super(details);
        this.gameServerIp = details.gameServerIp;
        this.gameServerPort = details.gameServerPort;
        this.key = details.key;
    }

    toObject(): x8CPacketType {
        return {
            gameServerIp: this.gameServerIp,
            gameServerPort: this.gameServerPort,
            key: this.key,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(11, unsafe);
        const serverIp = this.gameServerIp
            .split(".")
            .map((ipPart: string) => parseInt(ipPart));
        data.writeUInt8(x8CPacket.command);

        data.writeUInt8(serverIp[0], 1);
        data.writeUInt8(serverIp[1], 2);
        data.writeUInt8(serverIp[2], 3);
        data.writeUInt8(serverIp[3], 4);

        data.writeUInt16BE(this.gameServerPort, 5);

        data.writeUInt32BE(this.key, 7);

        return data;
    }

    static fromBuffer = (buffer: Buffer): x8CPacket => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new x8CPacket({
            gameServerIp: [
                buffer.readUInt8(1),
                buffer.readUInt8(2),
                buffer.readUInt8(3),
                buffer.readUInt8(4),
            ].join("."),
            gameServerPort: buffer.readUInt16BE(5),
            key: buffer.readUInt32BE(7),
        });
    };
}
