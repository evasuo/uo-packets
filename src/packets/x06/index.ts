import AbstractPacket, { PacketDataType } from "../AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface x06PacketType extends PacketDataType {
    clickedObject: number;
}

export default class x06Packet extends AbstractPacket {
    get clickedObject(): number {
        return this._clickedObject;
    }

    set clickedObject(value: number) {
        this._clickedObject = value;
    }
    static command = 0x06;
    static description = "Double Click";

    private _clickedObject: number;

    constructor(details: x06PacketType) {
        super(details);
        this.clickedObject = details.clickedObject;
    }

    toObject(): x06PacketType {
        return {
            clickedObject: this.clickedObject,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(3, unsafe);
        data.writeUInt8(x06Packet.command);
        data.writeUInt32BE(this.clickedObject, 1);

        return data;
    }

    static fromBuffer = (buffer: Buffer): x06Packet => {
        buffer = CrossCompatibleBuffer.from(Buffer);
        return new x06Packet({
            clickedObject: buffer.readUInt32BE(1),
        });
    };
}
