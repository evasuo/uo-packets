import { expect } from "chai";
import xB9Packet, { xB9PacketType } from "../../../src/packets/xB9";

describe("xB9", () => {
    it("validates packet info", () => {
        expect(() => new xB9Packet({ featureBitFlag: 0 })).to.throw(
            "clientVersion (string) or isLegacyClient (boolean) must be provided."
        );
    });
    it("clientVersion can be set", () => {
        const clientVersion = "3";
        const xB9Data = new xB9Packet({
            featureBitFlag: 0,
            clientVersion: clientVersion,
        });

        expect(xB9Data.clientVersion).to.eql(clientVersion);
    });
    it("toBuffer() can output non-legacy buffer", () => {
        const xB9 = new Promise<xB9Packet>((resolve) =>
            resolve(
                new xB9Packet({
                    featureBitFlag:
                        xB9Packet.FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT,
                    isLegacyClient: false,
                })
            )
        );

        return xB9.then((packet) => {
            expect(packet.toBuffer().length).to.eql(
                5,
                "Non-legacy buffer for this packet should have a length of 5"
            );
            expect(packet.toBuffer().readUInt8(0)).to.eql(
                xB9Packet.command,
                `Expected first byte to be command ${xB9Packet.command}`
            );
            expect(packet.toBuffer().readUInt32BE(1)).to.eql(
                xB9Packet.FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT,
                `Expected flag value to be ${xB9Packet.FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT}`
            );
        });
    });
    it("toBuffer() can output legacy buffer", () => {
        const xB9 = new Promise<xB9Packet>((resolve) =>
            resolve(
                new xB9Packet({
                    featureBitFlag: xB9Packet.FLAG_ENABLE_T2A_FEATURES,
                    isLegacyClient: true,
                })
            )
        );

        return xB9.then((packet) => {
            expect(packet.toBuffer().length).to.eql(
                3,
                "Legacy buffer for this packet should have a length of 3"
            );
            expect(packet.toBuffer().readUInt8(0)).to.eql(
                xB9Packet.command,
                `Expected first byte to be command ${xB9Packet.command}`
            );
            expect(packet.toBuffer().readUInt16BE(1)).to.eql(
                xB9Packet.FLAG_ENABLE_T2A_FEATURES,
                `Expected flag value to be ${xB9Packet.FLAG_ENABLE_T2A_FEATURES}`
            );
        });
    });
    it("appendFlag()", () => {
        const flags = 0;
        const xB9Data = new xB9Packet({
            featureBitFlag: flags,
            isLegacyClient: false,
        });

        xB9Data.addFlag(xB9Packet.FLAG_ENABLE_AOS_FEATURES);

        const { featureBitFlag: flagResult } = xB9Data.toObject();

        expect(flagResult | xB9Packet.FLAG_ENABLE_AOS_FEATURES).to.eql(
            xB9Packet.FLAG_ENABLE_AOS_FEATURES
        );
    });
    it("removeFlag()", () => {
        const testRemoveFlag = xB9Packet.FLAG_ENABLE_GOTHIC_HOUSING_TILES;
        const baseFlags =
            xB9Packet.FLAG_ENABLE_AOS_FEATURES |
            xB9Packet.FLAG_ENABLE_T2A_FEATURES;
        const flags = baseFlags | testRemoveFlag;

        const xB9Data = new xB9Packet({
            featureBitFlag: flags,
            isLegacyClient: false,
        });

        xB9Data.removeFlag(xB9Packet.FLAG_ENABLE_GOTHIC_HOUSING_TILES);

        const { featureBitFlag: remainingFlags } = xB9Data.toObject();

        expect(remainingFlags).to.eql(baseFlags);
    });
    it("Max feature flag value is 32-bit unsigned value", () => {
        const max32BitUnsignedInt = 4294967295;
        const xB9Data = new xB9Packet({
            featureBitFlag: max32BitUnsignedInt + 1,
            isLegacyClient: false,
        });

        const { featureBitFlag: flags } = xB9Data.toObject();

        expect(flags <= max32BitUnsignedInt).to.true;
    });
    it("Min feature flag value is 32-bit unsigned value", () => {
        const min32BitUnsignedInt = 0;
        const xB9Data = new xB9Packet({
            featureBitFlag: min32BitUnsignedInt - 1,
            isLegacyClient: false,
        });

        const { featureBitFlag: flags } = xB9Data.toObject();

        expect(flags >= min32BitUnsignedInt).to.true;
    });
    it("Toggles detects legacy based on client version", () => {
        const firstNonLegacyVersion = xB9Packet.firstNonLegacyClientVersion;
        const legacyVersion = firstNonLegacyVersion.replace(/^(\d)*\./, "1.");
        const xB9Data = new xB9Packet({
            featureBitFlag: 0,
            clientVersion: legacyVersion,
        });

        expect(xB9Data.toObject().isLegacyClient).to.be.true;
    });
    it("fromBuffer() can init class", () => {
        const xB9NonLegacyData = new xB9Packet({
            featureBitFlag: xB9Packet.FLAG_ENABLE_T2A_FEATURES,
            isLegacyClient: false,
        });
        const xB9LegacyData = new xB9Packet({
            featureBitFlag: xB9Packet.FLAG_ENABLE_T2A_FEATURES,
            isLegacyClient: true,
        });

        const resultsPromise = new Promise<
            [nonLegacy: xB9Packet, legacy: xB9Packet]
        >((resolve) => {
            resolve([
                xB9Packet.fromBuffer(xB9NonLegacyData.toBuffer()),
                xB9Packet.fromBuffer(xB9LegacyData.toBuffer()),
            ]);
        });

        return resultsPromise.then((results) => {
            const [nonLegacyResult, legacyResult] = results;
            expect(
                nonLegacyResult.toBuffer().length,
                "Non-legacy buffer should be 5 bytes"
            ).to.eql(xB9Packet.newClientLength);

            expect(
                legacyResult.toBuffer().length,
                "Legacy buffer should be 3 bytes"
            ).to.eql(xB9Packet.legacyLength);

            expect(nonLegacyResult.toObject().featureBitFlag).to.eql(
                xB9NonLegacyData.featureBitFlag
            );
            expect(legacyResult.toObject().featureBitFlag).to.eql(
                xB9LegacyData.featureBitFlag
            );

            const badCommand = xB9NonLegacyData.toBuffer();
            badCommand.writeUInt8(0x00);
            expect(() => xB9Packet.fromBuffer(badCommand)).to.throw(
                "Invalid packet provided. 0x00 does not match 0xB9"
            );

            const bufferTooLong = Buffer.alloc(6);
            bufferTooLong.writeUInt8(xB9Packet.command);
            expect(() => xB9Packet.fromBuffer(bufferTooLong)).to.throw(
                "This packet must be 3 bytes (Clients < 6.0.14.2) or 5 bytes (Clients >= 6.0.14.2"
            );
            const bufferTooShort = Buffer.alloc(2);
            bufferTooShort.writeUInt8(xB9Packet.command);
            expect(() => xB9Packet.fromBuffer(bufferTooShort)).to.throw(
                "This packet must be 3 bytes (Clients < 6.0.14.2) or 5 bytes (Clients >= 6.0.14.2"
            );
        });
    });

    it("toString()", () => {
        const details: xB9PacketType = {
            featureBitFlag: xB9Packet.FLAG_ENABLE_T2A_FEATURES,
            isLegacyClient: true,
        };
        const xB9Data = new xB9Packet(details);
        expect(JSON.parse(xB9Data.toString())).to.eql({
            ...details,
            features: xB9Packet.collectFlags(details.featureBitFlag),
        });
    });
});
