import AbstractPacket, { PacketDataType } from "../AbstractPacket";
import Version from "painless-version";
import Numbers from "../../helper/Numbers";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface xB9PacketType extends PacketDataType {
    featureBitFlag: number;
    clientVersion?: string;
    isLegacyClient?: boolean;
}

function isLegacyClient(clientVersion: string | number) {
    return Version.test(
        `${clientVersion} < ${xB9Packet.firstNonLegacyClientVersion}`
    );
}

type ClientFeatureFlags = string;
/**
 * Enable locked client features - http://docs.polserver.com/packets/index.php?Packet=0xB9
 * Sent by: Server
 * Size: 3 (Legacy Client)
 * Size: 5 bytes (Client 6.0.14.2+)
 */
export default class xB9Packet extends AbstractPacket {
    static command = 0xb9;
    static firstNonLegacyClientVersion = "6.0.14.2";
    static description = "Enable locked client features";
    static legacyLength = 3;
    static newClientLength = 5;

    /** Feature Flags */
    static FLAG_ENABLE_T2A_FEATURES = 0x01; // enable T2A features: chat, regions
    static FLAG_ENABLE_RENAISSANCE_FEATURES = 0x02; // enable renaissance features
    static FLAG_ENABLE_THIRD_DAWN_FEATURES = 0x04; // enable third dawn features
    static FLAG_ENABLE_LBR_FEATURES = 0x08; // enable LBR features: skills, map
    static FLAG_ENABLE_AOS_FEATURES = 0x10; // enable AOS features: skills, map, spells, fightbook
    static FLAG_ENABLE_6TH_CHARACTER_SLOT = 0x20; // 6th character slot
    static FLAG_ENABLE_SE_FEATURES = 0x40; // enable SE features
    static FLAG_ENABLE_ML_FEATURES = 0x80; // enable ML features: elven race, spells, skills
    static FLAG_ENABLE_8TH_AGE_SPLASH_SCREEN = 0x100; // enable 8th age splash screen
    static FLAG_ENABLE_9TH_AGE_SPLASH_SCREEN = 0x200; // enable 9th age splash screen
    static FLAG_ENABLE_10TH_AGE_SPLASH_SCREEN = 0x400; // enable 10th age
    static FLAG_ENABLE_INCREASED_HOUSING_BANK_STORAGE = 0x800; // enable increased housing and bank storage
    static FLAG_ENABLE_7TH_CHARACTER_SLOT = 0x1000; // 7th character slot
    static FLAG_ENABLE_KR_FACES = 0x2000; // enable KR faces
    static FLAG_ENABLE_TRIAL_ACCOUNT = 0x4000; // enable trial account
    static FLAG_ENABLE_LIVE_ACCOUNT = 0x8000; // enable live account
    static FLAG_ENABLE_SA_FEATURES = 0x10000; // enable SA features: gargoyle race, spells, skills
    static FLAG_ENABLE_HSA_FEATURES = 0x20000; // enable HSA features
    static FLAG_ENABLE_GOTHIC_HOUSING_TILES = 0x40000; // enable Gothic housing tiles
    static FLAG_ENABLE_RUSTIC_HOUSING_TILES = 0x80000; // enable Rustic housing tiles
    static FLAG_ENABLE_JUNGLE_HOUSING_TILES = 0x100000; // enable Jungle housing tiles
    static FLAG_ENABLE_SHADOWGAURD_HOUSING_TILES = 0x200000; // enabled Shadowguard housing tiles
    static FLAG_ENABLE_TOL_FEATURES = 0x400000; // enable TOL features
    static FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT = 0x800000; // enable Endless Journey account

    static collectFlags = (flags: number): ClientFeatureFlags[] => {
        const availableFlags = {};
        availableFlags[xB9Packet.FLAG_ENABLE_T2A_FEATURES] =
            "ENABLE_T2A_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_RENAISSANCE_FEATURES] =
            "ENABLE_RENAISSANCE_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_THIRD_DAWN_FEATURES] =
            "ENABLE_THIRD_DAWN_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_LBR_FEATURES] =
            "FLAG_ENABLE_LBR_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_AOS_FEATURES] =
            "FLAG_ENABLE_AOS_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_6TH_CHARACTER_SLOT] =
            "FLAG_ENABLE_6TH_CHARACTER_SLOT";
        availableFlags[xB9Packet.FLAG_ENABLE_SE_FEATURES] =
            "FLAG_ENABLE_SE_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_ML_FEATURES] =
            "FLAG_ENABLE_ML_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_8TH_AGE_SPLASH_SCREEN] =
            "FLAG_ENABLE_8TH_AGE_SPLASH_SCREEN";
        availableFlags[xB9Packet.FLAG_ENABLE_9TH_AGE_SPLASH_SCREEN] =
            "FLAG_ENABLE_9TH_AGE_SPLASH_SCREEN";
        availableFlags[xB9Packet.FLAG_ENABLE_10TH_AGE_SPLASH_SCREEN] =
            "FLAG_ENABLE_10TH_AGE_SPLASH_SCREEN";
        availableFlags[xB9Packet.FLAG_ENABLE_INCREASED_HOUSING_BANK_STORAGE] =
            "FLAG_ENABLE_INCREASED_HOUSING_BANK_STORAGE";
        availableFlags[xB9Packet.FLAG_ENABLE_7TH_CHARACTER_SLOT] =
            "FLAG_ENABLE_7TH_CHARACTER_SLOT";
        availableFlags[xB9Packet.FLAG_ENABLE_KR_FACES] = "FLAG_ENABLE_KR_FACES";
        availableFlags[xB9Packet.FLAG_ENABLE_TRIAL_ACCOUNT] =
            "FLAG_ENABLE_TRIAL_ACCOUNT";
        availableFlags[xB9Packet.FLAG_ENABLE_LIVE_ACCOUNT] =
            "FLAG_ENABLE_LIVE_ACCOUNT";
        availableFlags[xB9Packet.FLAG_ENABLE_SA_FEATURES] =
            "FLAG_ENABLE_SA_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_HSA_FEATURES] =
            "FLAG_ENABLE_HSA_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_GOTHIC_HOUSING_TILES] =
            "FLAG_ENABLE_GOTHIC_HOUSING_TILES";
        availableFlags[xB9Packet.FLAG_ENABLE_RUSTIC_HOUSING_TILES] =
            "FLAG_ENABLE_RUSTIC_HOUSING_TILES";
        availableFlags[xB9Packet.FLAG_ENABLE_JUNGLE_HOUSING_TILES] =
            "FLAG_ENABLE_JUNGLE_HOUSING_TILES";
        availableFlags[xB9Packet.FLAG_ENABLE_SHADOWGAURD_HOUSING_TILES] =
            "FLAG_ENABLE_SHADOWGAURD_HOUSING_TILES";
        availableFlags[xB9Packet.FLAG_ENABLE_TOL_FEATURES] =
            "FLAG_ENABLE_TOL_FEATURES";
        availableFlags[xB9Packet.FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT] =
            "FLAG_ENABLE_ENDLESS_JOURNEY_ACCOUNT";

        return Object.keys(availableFlags)
            .filter((flag) => (Number(flag) & flags) === Number(flag))
            .map((matchingFlag) => availableFlags[matchingFlag]);
    };

    constructor(details: xB9PacketType) {
        super(details);

        xB9Packet.validatePacketInfo(details);

        if (typeof details.isLegacyClient === "boolean")
            this.isLegacy = details.isLegacyClient;
        else {
            this.isLegacy = isLegacyClient(details.clientVersion);
            this.clientVersion = details.clientVersion.toString();
        }

        this.featureBitFlag = details.featureBitFlag;
    }

    private _isLegacy: boolean;

    get isLegacy(): boolean {
        return this._isLegacy;
    }

    set isLegacy(value: boolean) {
        this._isLegacy = value;
    }

    private _featureBitFlag: number;

    get featureBitFlag(): number {
        return this._featureBitFlag;
    }

    set featureBitFlag(value: number) {
        this._featureBitFlag = Numbers.toUnsignedInt(value);
    }

    private _clientVersion: string;

    get clientVersion(): string {
        return this._clientVersion;
    }

    set clientVersion(value: string) {
        this.isLegacy = isLegacyClient(value);
        this._clientVersion = value;
    }

    static fromBuffer = (buffer: Buffer): xB9Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        const isValidBuffer = buffer.readUInt8() !== xB9Packet.command;
        if (isValidBuffer)
            throw Error(
                `Invalid packet provided. 0x${buffer
                    .slice(0, 1)
                    .toString("hex")} does not match 0xB9`
            );

        if (!xB9Packet.isValidBufferLength(buffer))
            throw Error(
                "This packet must be 3 bytes (Clients < 6.0.14.2) or 5 bytes (Clients >= 6.0.14.2"
            );

        return new xB9Packet({
            featureBitFlag:
                buffer.length === 3
                    ? buffer.readUInt16BE(1)
                    : buffer.readUInt32BE(1),
            isLegacyClient: buffer.length === xB9Packet.legacyLength,
        });
    };

    private static isValidBufferLength(buffer: Buffer) {
        return (
            buffer.length === xB9Packet.newClientLength ||
            buffer.length === xB9Packet.legacyLength
        );
    }

    addFlag(flag: number): xB9Packet {
        this.featureBitFlag = this.featureBitFlag | flag;

        return this;
    }

    removeFlag(flag: number): xB9Packet {
        this.featureBitFlag = this.featureBitFlag ^ flag;

        return this;
    }

    toObject(): xB9PacketType {
        return {
            featureBitFlag: this.featureBitFlag,
            isLegacyClient: this.isLegacy,
            features: xB9Packet.collectFlags(this.featureBitFlag),
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(
            this.isLegacy ? xB9Packet.legacyLength : xB9Packet.newClientLength,
            unsafe
        );
        data.writeUInt8(xB9Packet.command);

        if (this.isLegacy) data.writeUInt16BE(this.featureBitFlag, 1);
        else data.writeUInt32BE(this.featureBitFlag, 1);

        return data;
    }

    private static validatePacketInfo(details: xB9PacketType) {
        if (
            typeof details.isLegacyClient !== "boolean" &&
            typeof details.clientVersion !== "string"
        )
            throw Error(
                "clientVersion (string) or isLegacyClient (boolean) must be provided."
            );
    }
}
