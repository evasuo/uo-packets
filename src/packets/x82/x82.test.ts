import { expect } from "chai";
import x82Packet from "./index";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer;

describe("x82", () => {
    it("has single-byte command", () =>
        expect(Buffer.from([x82Packet.command]).length).to.eql(1));

    it("toObject()", () => {
        const x82 = new x82Packet({ reason: x82Packet.REASON_BAD_PASSWORD });
        const resultPromise = new Promise<x82Packet>((resolve) => resolve(x82));

        return resultPromise.then((result) => {
            expect(result.toObject()).to.haveOwnProperty("reason");
            expect(result.toObject().reason).to.eql(
                x82Packet.REASON_BAD_PASSWORD
            );
        });
    });

    it("toString()", () => {
        const x82 = new x82Packet({ reason: x82Packet.REASON_BAD_PASSWORD });
        const resultPromise = new Promise<x82Packet>((resolve) => resolve(x82));

        return resultPromise.then((result) => {
            expect(result.toString()).to.eql(JSON.stringify(result.toObject()));
        });
    });

    it("toBuffer()", () => {
        const x82 = new x82Packet({ reason: x82Packet.REASON_BAD_PASSWORD });
        const resultPromise = new Promise<x82Packet>((resolve) => resolve(x82));

        return resultPromise.then((result) => {
            const buffer = result.toBuffer();
            expect(
                buffer,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(CrossCompatibleBuffer);

            expect(
                buffer.readUInt8(0),
                "First byte should match command"
            ).to.eql(x82Packet.command);

            expect(
                buffer.readUInt16BE(1),
                "Reason in buffer does not match what was provided"
            ).to.eql(x82Packet.REASON_BAD_PASSWORD);
        });
    });

    it("fromBuffer()", () => {
        const x82 = new x82Packet({ reason: x82Packet.REASON_BAD_PASSWORD });
        const resultPromise = new Promise<x82Packet>((resolve) => resolve(x82));

        return resultPromise.then((result) => {
            const srcBuffer = result.toBuffer();
            const x82Data = x82Packet.fromBuffer(srcBuffer);

            expect(
                x82Data,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(x82Packet);

            expect(x82Data.command, "First byte should match command").to.eql(
                x82Packet.command
            );

            expect(
                x82Data.reason,
                "Reason in data does not match what was provided"
            ).to.eql(x82Packet.REASON_BAD_PASSWORD);
        });
    });
});
