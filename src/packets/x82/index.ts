import AbstractPacket, { PacketDataType } from "../AbstractPacket";
import Numbers from "../../helper/Numbers";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

export interface x82PacketType extends PacketDataType {
    reason: number;
}

export default class x82Packet extends AbstractPacket {
    static command = 0x82;
    static description = "Login denied";
    static REASON_INVALID_CREDENTIALS = 0;
    static REASON_IN_USE = 1;
    static REASON_BLOCKED = 2;
    static REASON_BAD_PASSWORD = 3;
    static REASON_COMMUNICATION_ISSUE = 4;
    static REASON_IGR_CONCURRENCY_LIMIT_MET = 5;
    static REASON_IGR_CONCURRENCY_TIME_LIMIT_MET = 6;
    static REASON_IGR_GENERAL_AUTHENTICATION_FAILURE = 7;
    static REASON_IDLE = 254;
    static REASON_BAD_COMM = 255;
    readonly command = x82Packet.command;
    readonly description = x82Packet.description;

    constructor(details: x82PacketType) {
        super(details);
        this.reason = details.reason;
    }

    private _reason: number;

    get reason(): number {
        return this._reason;
    }

    set reason(value: number) {
        Numbers.ValidateUInt8(value);
        this._reason = Numbers.toUInt8(value);
    }

    static fromBuffer = (buffer: Buffer): x82Packet => {
        buffer = CrossCompatibleBuffer.from(buffer);
        return new x82Packet({
            reason: buffer.readUInt16BE(1),
        });
    };

    toObject(): x82PacketType {
        return {
            reason: this.reason,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        const data = AbstractPacket.createBuffer(3, unsafe);
        data.writeUInt8(x82Packet.command);
        data.writeUInt16BE(this.reason, 1);

        return data;
    }
}
