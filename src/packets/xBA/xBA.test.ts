import { expect } from "chai";
import xBAPacket from "./index";
import xB9Packet from "../xB9";
// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer;

function multiExpect(xBA: xBAPacket) {
    return new Promise<xBAPacket>((resolve) => resolve(xBA));
}

describe("xBA", () => {
    const serialDetails = {
        active: true,
        location: { x: 1, y: 3 },
        serial: 50,
    };
    const noSerialDetails = {
        active: true,
        location: { x: 1, y: 3 },
    };
    it("has single-byte command", () =>
        expect(Buffer.from([xBAPacket.command]).length).to.eql(1));

    it("toObject()", () => {
        const xBA = new xBAPacket(serialDetails);
        const resultPromise = new Promise<xBAPacket>((resolve) => resolve(xBA));

        return resultPromise.then((result) => {
            expect(result.toObject()).to.haveOwnProperty("active");
            expect(result.toObject()).to.haveOwnProperty("location");
            expect(result.toObject()).to.haveOwnProperty("serial");
            expect(result.toObject()).to.be.eql(serialDetails);
        });
    });

    it("toString()", () => {
        const xBA = new xBAPacket(serialDetails);

        expect(xBA.toString()).to.eql(JSON.stringify(xBA.toObject()));
    });

    it("toBuffer()", () => {
        const xBA = new xBAPacket(serialDetails);

        return multiExpect(xBA).then((result) => {
            const buffer = result.toBuffer();
            result.active = false;
            const bufferActiveFalse = result.toBuffer();

            expect(
                buffer,
                "Should be web-compatible AND nodejs compatible buffer"
            ).to.be.instanceOf(CrossCompatibleBuffer);

            expect(
                buffer.readUInt8(0),
                "First byte should match command"
            ).to.eql(xBAPacket.command);

            expect(
                Boolean(buffer.readUInt8(1)),
                "Active state should match when true"
            ).to.eql(serialDetails.active);

            expect(
                Boolean(bufferActiveFalse.readUInt8(1)),
                "Active state should match when false"
            ).to.eql(false);

            expect(buffer.readUInt16BE(2), "X location should match").to.eql(
                serialDetails.location.x
            );

            expect(buffer.readUInt16BE(4), "Y location should match").to.eql(
                serialDetails.location.y
            );

            expect(buffer.length).to.eql(xBAPacket.serialLength);
            result.serial = undefined;
            expect(result.toBuffer().length).to.eql(xBAPacket.noSerialLength);
        });
    });

    describe("fromBuffer()", () => {
        describe("Serial length", () => {
            const source = new xBAPacket({
                active: true,
                location: { y: 1, x: 2 },
                serial: 123,
            });
            const result = xBAPacket.fromBuffer(source.toBuffer());
            it("Is an instance of xBA Class", () => {
                expect(result).to.be.instanceOf(xBAPacket);
            });

            it("Has expected serial", () => {
                expect(result.serial).to.eql(source.serial);
            });
            it("Has expected active state", () => {
                expect(result.active).to.eql(source.active);
            });
            it("Has expected location", () => {
                expect(result.location).to.be.eql(source.location);
            });
        });

        describe("Non-serial length", () => {
            const source = new xBAPacket({
                active: false,
                location: { y: 1, x: 2 },
            });
            const result = xBAPacket.fromBuffer(source.toBuffer());
            it("Is an instance of xBA Class", () => {
                expect(result).to.be.instanceOf(xBAPacket);
            });

            it("Has expected serial", () => {
                expect(result.serial).to.to.be.undefined;
            });
            it("Has expected active state", () => {
                expect(result.active).to.eql(source.active);
            });
            it("Has expected location", () => {
                expect(result.location).to.be.eql(source.location);
            });
        });
    });
});
