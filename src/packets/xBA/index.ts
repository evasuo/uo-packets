import AbstractPacket, { PacketDataType } from "../AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

type xyLocation = { x: number; y: number };

export interface xBAPacketType extends PacketDataType {
    active: boolean;
    location: xyLocation;
    // If HSA
    serial?: number;
}

export default class xBAPacket extends AbstractPacket {
    static command = 0xba;
    readonly command = xBAPacket.command;
    static description = "Quest arrow";
    readonly description = xBAPacket.description;

    constructor(details: xBAPacketType) {
        super(details);
        this.active = details.active;
        this.location = details.location;
        // If HSA
        this.serial = details.serial;
    }

    private _active: boolean;

    get active(): boolean {
        return this._active;
    }

    set active(value: boolean) {
        this._active = value;
    }

    private _location: xyLocation;

    get location(): xyLocation {
        return this._location;
    }

    set location(value: xyLocation) {
        this._location = value;
    }

    private _serial: number;

    get serial(): number {
        return this._serial;
    }

    set serial(value: number) {
        this._serial = value;
    }

    static readonly serialLength = 10;
    static readonly noSerialLength = 6;
    static fromBuffer = (buffer: Buffer): xBAPacket => {
        buffer = CrossCompatibleBuffer.from(buffer);
        const details: xBAPacketType = {
            active: Boolean(buffer.readUInt8(1)),
            location: {
                x: buffer.readUInt16BE(2),
                y: buffer.readUInt16BE(4),
            },
        };
        if (buffer.length === xBAPacket.serialLength)
            details.serial = buffer.readUInt32BE(6);

        return new xBAPacket(details);
    };

    toObject(): xBAPacketType {
        return {
            active: this.active,
            location: this.location,
            // If HSA
            serial: this.serial,
        };
    }

    toString(): string {
        return JSON.stringify(this.toObject());
    }

    toBuffer(unsafe = false): Buffer {
        let length = xBAPacket.noSerialLength;
        if (this.serial) length = xBAPacket.serialLength;

        const data = AbstractPacket.createBuffer(length, unsafe);
        data.writeUInt8(this.command);
        data.writeUInt8(this.active ? 1 : 0, 1);
        data.writeUInt16BE(this.location.x, 2);
        data.writeUInt16BE(this.location.y, 4);

        if (this.serial) data.writeUInt32BE(this.serial, 6);

        return data;
    }
}
