import xA8 from "./packets/xA8";
import xEF from "./packets/xEF";
import x8C from "./packets/x8C";
import xA0 from "./packets/xA0";
import xB9 from "./packets/xB9";
import xBA from "./packets/xBA";
import AbstractPacket from "./packets/AbstractPacket";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const CrossCompatibleBuffer = require("buffer/").Buffer; // note: the trailing slash is important!

const Packets = {
    fromBuffer: (buffer: Buffer): AbstractPacket => {
        buffer = CrossCompatibleBuffer.from(buffer);
        const packetCommand = buffer.readUInt8();
        const packet = Packets[packetCommand];

        if (packet) return packet.fromBuffer(buffer);

        throw new Error(
            `Packet ${packetCommand} (0x${buffer
                .slice(0, 1)
                .toString("hex")}) is not be found.`
        );
    },
};

Packets[xA8.command] = xA8;
Packets[xEF.command] = xEF;
Packets[x8C.command] = x8C;
Packets[xA0.command] = xA0;
Packets[xBA.command] = xBA;
Packets[xB9.command] = xB9;

export default Packets;
