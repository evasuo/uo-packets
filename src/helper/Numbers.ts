export type UInt8 = number; // 🙏 In hopes that one day TypeScript will support this
export type UInt16 = number; // 🙏 In hopes that one day TypeScript will support this
export type UInt32 = number; // 🙏 In hopes that one day TypeScript will support this

const toUnsignedInt = (number: number): UInt32 => {
    return number >>> 0;
};

const toUInt32 = toUnsignedInt;

const toUInt16 = (number: number): UInt16 => {
    return toUInt32(number) & 0xffff;
};

const toUInt8 = (number: number): UInt8 => {
    return toUInt32(number) & 0xff;
};

const ValidateUInt8 = (number: number, message?: string) => {
    if (toUInt8(number) !== number)
        throw Error(
            message ? message : `Expected UInt8 number (0..255), got ${number}`
        );
};

const ValidateUInt16 = (number: number, message?: string) => {
    if (toUInt16(number) !== number)
        throw Error(
            message
                ? message
                : `Expected UInt16 number (0..65535), got ${number}`
        );
};

export default {
    toUnsignedInt,
    toUInt32,
    toUInt16,
    toUInt8,
    ValidateUInt8,
    ValidateUInt16,
};
