import Numbers from "./Numbers";
import { expect } from "chai";

function toUInt32Tests(unsignedInt32Method = "toUnsignedInt") {
    it("Should return unsigned int", () => {
        const max = 4294967295,
            min = 0;

        const resultPromise = new Promise<[max: number, min: number]>(
            (resolve) => {
                resolve([
                    Numbers[unsignedInt32Method](max + 1),
                    Numbers[unsignedInt32Method](min - 1),
                ]);
            }
        );

        return resultPromise.then((result) => {
            const [maxResult, minResult] = result;
            expect(
                maxResult <= max,
                `Max value should be ${max} but got ${maxResult}`
            ).to.be.true;
            expect(
                minResult >= min,
                `Min value should be ${max} but got ${maxResult}`
            ).to.be.true;
        });
    });

    it("Should wrap max overflow", () => {
        const max = 4294967295;
        const overflow = 20;

        const unsignedInt = Numbers[unsignedInt32Method](max + overflow);

        expect(unsignedInt).to.eql(overflow - 1);
    });

    it("Should wrap min underflow", () => {
        const unsignedNumberMax = 4294967295;
        const unsignedNumberMin = 0;
        const underflow = 20;

        const unsignedInt = Numbers[unsignedInt32Method](
            unsignedNumberMin - underflow
        );

        expect(unsignedInt).to.eql(unsignedNumberMax - underflow + 1);
    });
}

describe("Numbers.toUnsignedInt()", () => {
    toUInt32Tests();
});

describe("Numbers.toUnsignedInt32()", () => {
    toUInt32Tests("toUInt32");
});

describe("Numbers.toUInt16()", () => {
    it("Should return unsigned 16-bit int", () => {
        const max = 65535,
            min = 0;

        const resultPromise = new Promise<[max: number, min: number]>(
            (resolve) => {
                resolve([Numbers.toUInt16(max + 1), Numbers.toUInt16(min - 1)]);
            }
        );

        return resultPromise.then((result) => {
            const [maxResult, minResult] = result;
            expect(
                maxResult <= max,
                `Max value should be ${max} but got ${maxResult}`
            ).to.be.true;
            expect(
                minResult >= min,
                `Min value should be ${max} but got ${maxResult}`
            ).to.be.true;
        });
    });

    it("Should wrap max overflow", () => {
        const max = 65535;
        const overflow = 20;

        const unsignedInt = Numbers.toUInt16(max + overflow);

        expect(unsignedInt).to.eql(overflow - 1);
    });

    it("Should wrap min underflow", () => {
        const unsignedNumberMax = 65535;
        const unsignedNumberMin = 0;
        const underflow = 20;

        const unsignedInt = Numbers.toUInt16(unsignedNumberMin - underflow);

        expect(unsignedInt).to.eql(unsignedNumberMax - underflow + 1);
    });
});

describe("Numbers.toUInt8()", () => {
    it("Should return unsigned 16-bit int", () => {
        const max = 255,
            min = 0;

        const resultPromise = new Promise<[max: number, min: number]>(
            (resolve) => {
                resolve([Numbers.toUInt8(max + 1), Numbers.toUInt8(min - 1)]);
            }
        );

        return resultPromise.then((result) => {
            const [maxResult, minResult] = result;
            expect(
                maxResult <= max,
                `Max value should be ${max} but got ${maxResult}`
            ).to.be.true;
            expect(
                minResult >= min,
                `Min value should be ${max} but got ${maxResult}`
            ).to.be.true;
        });
    });

    it("Should wrap max overflow", () => {
        const max = 255;
        const overflow = 20;

        const unsignedInt = Numbers.toUInt8(max + overflow);

        expect(unsignedInt).to.eql(overflow - 1);
    });

    it("Should wrap min underflow", () => {
        const unsignedNumberMax = 255;
        const unsignedNumberMin = 0;
        const underflow = 20;

        const unsignedInt = Numbers.toUInt8(unsignedNumberMin - underflow);

        expect(unsignedInt).to.eql(unsignedNumberMax - underflow + 1);
    });
});

describe("Numbers.ValidateUInt8()", () => {
    it("Throws error on numbers that are not unsigned 8-bit integers", () => {
        expect(() => Numbers.ValidateUInt8(256)).to.throw(
            "Expected UInt8 number (0..255), got 256"
        );
    });
    it("Throws custom error message when provided", () => {
        expect(() =>
            Numbers.ValidateUInt8(256, "Custom Error Message")
        ).to.throw("Custom Error Message");
    });
});

describe("Numbers.ValidateUInt16()", () => {
    it("Throws error on numbers that are not unsigned 16-bit integers", () => {
        expect(() => Numbers.ValidateUInt16(-1)).to.throw(
            "Expected UInt16 number (0..65535), got -1"
        );
    });
    it("Throws custom error message when provided", () => {
        expect(() =>
            Numbers.ValidateUInt16(-1, "Custom Error Message")
        ).to.throw("Custom Error Message");
    });
});
