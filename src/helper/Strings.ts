export type AsciiStringType = string; // 🙏 One day

export const AsciiTrim = (value: string) => {
    return value.trim().replace(/\0*$/g, "");
};

export default {
    AsciiTrim,
};
